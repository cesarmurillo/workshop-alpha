<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name_job');
            $table->text('resume_job')->nullable();
            $table->text('specs_job')->nullable();
            $table->text('condition_job')->nullable();
            $table->text('duty_job')->nullable();
            $table->text('responsibility_job')->nullable();
            $table->text('training_job')->nullable();

            $table->unsignedInteger('area_id')->nullable();
            $table->foreign('area_id')->references('id')->on('areas');

            $table->integer('user_create')->nullable();
            $table->integer('user_update')->nullable();
            $table->integer('user_delete')->nullable();
            $table->string('host_create', '15')->nullable();
            $table->string('host_update', '15')->nullable();
            $table->string('host_delete', '15')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
