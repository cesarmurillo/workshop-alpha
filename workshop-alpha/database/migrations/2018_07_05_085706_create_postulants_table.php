<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostulantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postulants', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');

            $table->unsignedInteger('person_id');
            $table->foreign('person_id')->references('id')->on('people');

            $table->unsignedInteger('requirement_id');
            $table->foreign('requirement_id')->references('id')->on('requirements');

            $table->integer('user_create')->nullable();
            $table->integer('user_update')->nullable();
            $table->integer('user_delete')->nullable();
            $table->string('host_create', '15')->nullable();
            $table->string('host_update', '15')->nullable();
            $table->string('host_delete', '15')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postulants');
    }
}
