<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdentificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('identifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type_identification');
            $table->integer('number_identification');
            $table->string('add_identification', '4')->nullable();
            $table->string('expedition_place');
            $table->date('expedition_date');
            $table->date('expiration_date');

            $table->unsignedInteger('person_id');
            $table->foreign('person_id')->references('id')->on('people');

            $table->integer('user_create')->nullable();
            $table->integer('user_update')->nullable();
            $table->integer('user_delete')->nullable();
            $table->string('host_create', '15')->nullable();
            $table->string('host_update', '15')->nullable();
            $table->string('host_delete', '15')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identifications');
    }
}
