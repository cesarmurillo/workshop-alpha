<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDependentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dependents', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name_dependent');
            $table->string('relation_dependent');
            $table->date('birth_date_dependent');
            $table->string('disability_dependent');

            $table->unsignedInteger('person_id');
            $table->foreign('person_id')->references('id')->on('people');

            $table->integer('user_create')->nullable();
            $table->integer('user_update')->nullable();
            $table->integer('user_delete')->nullable();
            $table->string('host_create', '15')->nullable();
            $table->string('host_update', '15')->nullable();
            $table->string('host_delete', '15')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dependents');
    }
}
