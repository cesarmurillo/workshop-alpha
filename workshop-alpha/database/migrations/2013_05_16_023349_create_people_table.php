<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');

            $table->string('first_name', '25');
            $table->string('last_name', '25');
            $table->string('second_last_name', '25');
            $table->string('gender', '10')->nullable();
            $table->date('birth_date');
            $table->string('personal_email')->nullable();
            $table->integer('phone_number')->nullable();
            $table->integer('cell_number')->nullable();
            $table->string('address')->nullable();
            $table->string('birth_place')->nullable();
            $table->string('nacionality')->nullable();
            $table->string('civil_status')->nullable();
            $table->string('disability')->nullable();

            $table->integer('user_create')->nullable();
            $table->integer('user_update')->nullable();
            $table->integer('user_delete')->nullable();
            $table->string('host_create', '15')->nullable();
            $table->string('host_update', '15')->nullable();
            $table->string('host_delete', '15')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
