<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            'email_employee' => 'cmurillot@lexcorp.com',
            'entry_employee' => '2018/06/06',
            'salary_employee' => '2000',
            'job_id' => '1',
            'person_id' => '1'
        ]);
        DB::table('employees')->insert([
            'email_employee' => 'cmurillot@lexcorp.com',
            'entry_employee' => '2018/06/06',
            'salary_employee' => '2000',
            'job_id' => '1',
            'person_id' => '2'
        ]);
    }
}
