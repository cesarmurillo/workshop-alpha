<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobs')->insert([
            'name_job' => 'Gerente General',
            'area_id' => '1'
        ]);
        DB::table('jobs')->insert([
            'name_job' => 'Gerente de RRHH',
            'area_id' => '2'
        ]);
        DB::table('jobs')->insert([
            'name_job' => 'Gerente de Finanzas',
            'area_id' => '3'
        ]);
        DB::table('jobs')->insert([
            'name_job' => 'Gerente de Mercadotecnia',
            'area_id' => '4'
        ]);
        DB::table('jobs')->insert([
            'name_job' => 'Gerente de Sistemas',
            'area_id' => '5'
        ]);
    }
}
