<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('people')->insert([
            'first_name' => 'Cesar',
            'last_name' => 'Murillo',
            'second_last_name' => 'Torrez',
            'birth_date' => '1994/10/10'
        ]);
        DB::table('people')->insert([
            'first_name' => 'jose',
            'last_name' => 'perez',
            'second_last_name' => 'perez',
            'birth_date' => '1994/10/10'
        ]);
    }
}
