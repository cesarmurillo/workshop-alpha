<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'cmurillot',
            'password' => bcrypt('secret'),
            'fn' => true,
            'person_id' => '1'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '1',
            'role_id' => '2',
        ]);

        DB::table('users')->insert([
            'username' => 'jperezp',
            'password' => bcrypt('secret'),
            'fn' => true,
            'person_id' => '2'
        ]);
        DB::table('role_user')->insert([
            'user_id' => '2',
            'role_id' => '3',
        ]);
    }
}
