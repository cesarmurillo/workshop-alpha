<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('areas')->insert([
            'name_area' => 'Dirección General',
            'description_area' => 'None'
        ]);
        DB::table('areas')->insert([
            'name_area' => 'Administración y Recursos Humanos',
            'description_area' => 'None'
        ]);
        DB::table('areas')->insert([
            'name_area' => 'Finanzas y Contabilidad',
            'description_area' => 'None'
        ]);
        DB::table('areas')->insert([
            'name_area' => 'Publicidad y Mercadotecnia',
            'description_area' => 'None'
        ]);
        DB::table('areas')->insert([
            'name_area' => 'Informática',
            'description_area' => 'None'
        ]);
    }
}
