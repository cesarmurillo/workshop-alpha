<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'role' => 'Encargado de sistema',
        ]);
        DB::table('roles')->insert([
            'role' => 'Encargado de RRHH',
        ]);
        DB::table('roles')->insert([
            'role' => 'Jefe',
        ]);
        DB::table('roles')->insert([
            'role' => 'Empleado',
        ]);
        DB::table('roles')->insert([
            'role' => 'Invitado',
        ]);
    }
}
