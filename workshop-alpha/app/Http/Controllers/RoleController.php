<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestRole;
use App\Privilege;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('role.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $privileges = Privilege::all();
        return view('role.create', ['privileges' => $privileges]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestRole $request)
    {
        DB::transaction(function () use ($request) {
            $role = New Role($request->all());
            $role->host_create = $request->ip();
            $role->save();
            $role->privileges()->attach($request->privilege);

        });
        return redirect()->action('RoleController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        return view('role.show', ['role' => $role, 'privileges' => $role->privileges()->get()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $privilege = Privilege::all();
        return view('role.update', ['role' => $role, 'privileges' => $privilege]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(RequestRole $request, Role $role)
    {
        DB::transaction(function () use ($role, $request) {
            $role->update($request->all());
            $role->host_update = $request->ip();
            $role->save();
            $role->privileges()->sync($request->privilege);
        });
        return redirect()->action('RoleController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        DB::transaction(function () use ($role){
            $role->delete();
        });
        return redirect()->action('RoleController@index');
    }

    public function getRoleData()
    {
        $roles = Role::select(['id', 'role']);

        try {
            return Datatables::of($roles)
                ->addColumn('view', function ($role) {
                    return view('role.viewData', ['id' => $role->id]);})
                ->addColumn('edit', function ($role) {
                    return view('role.editData', ['id' => $role->id]);})
                ->addColumn('delete', function ($role) {
                    return view('role.deleteData', ['id' => $role->id]);})
                ->rawColumns(['view', 'edit', 'delete'])->make();
        } catch (\Exception $e) {
        }
    }
}
