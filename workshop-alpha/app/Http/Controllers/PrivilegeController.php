<?php

namespace App\Http\Controllers;

use App\Area;
use App\Http\Requests\RequestPrivilege;
use App\Privilege;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class PrivilegeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('privilege.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('privilege.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestPrivilege $request)
    {
        DB::transaction(function () use ($request) {
            $privilege = new Privilege($request->all());
            $privilege->host_create = $request->ip();
            $privilege->save();
        });
        return redirect()->action('PrivilegeController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Privilege  $privilege
     * @return \Illuminate\Http\Response
     */
    public function show(Privilege $privilege)
    {
        return view('privilege.show', ['privilege' => $privilege]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Privilege  $privilege
     * @return \Illuminate\Http\Response
     */
    public function edit(Privilege $privilege)
    {
        return view('privilege.update', ['privilege' => $privilege]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Privilege  $privilege
     * @return \Illuminate\Http\Response
     */
    public function update(RequestPrivilege $request, Privilege $privilege)
    {
        DB::transaction(function () use ($privilege, $request) {
            $privilege->update($request->all());
            $privilege->host_update = $request->ip();
            $privilege->save();
        });
        return redirect()->action('PrivilegeController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Privilege $privilege
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Privilege $privilege)
    {
        DB::transaction(function () use ($privilege){
            $privilege->delete();
        });
        return redirect()->action('PrivilegeController@index');
    }

    public function getPrivilegeData()
    {
        $privileges = Privilege::select(['id', 'privilege']);

        try {
            return Datatables::of($privileges)
                ->addColumn('view', function ($privilege) {
                    return view('privilege.viewData', ['id' => $privilege->id]);})
                ->addColumn('edit', function ($privilege) {
                    return view('privilege.editData', ['id' => $privilege->id]);})
                ->addColumn('delete', function ($privilege) {
                    return view('privilege.deleteData', ['id' => $privilege->id]);})
                ->rawColumns(['view', 'edit', 'delete'])->make();
        } catch (\Exception $e) {
        }
    }
}
