<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestUser;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        $role = DB::table('roles')
            ->join('role_user', 'roles.id', '=', 'role_user.role_id')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->where('users.id', '=', $user->id)
            ->select('roles.id')
            ->whereNull('roles.deleted_at')
            ->get();
        if($role[0]->id == 4)
        {
            dd($role[0]->id);
        }
        else
        {
            dd($role);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role_privileges = DB::table('roles')
            ->join('privilege_role', 'roles.id', '=', 'privilege_role.role_id')
            ->join('privileges', 'privilege_role.privilege_id', '=', 'privileges.id')
            ->select('roles.id' , 'roles.role', 'privileges.privilege')
            ->whereNull('roles.deleted_at')
            ->groupBy('roles.id', 'roles.role', 'privileges.privilege')
            ->get();
        /*$roles = new Role();
        $roles->privileges()->toSql();*/
        /*$role_privileges = null;
        foreach ($roles as $role)
        {
            $role->privileges();
        }*/
        $collection = collect($role_privileges);
        $grouped = $collection->groupBy('role', true);

        //dd($grouped);
        return view('user.create', ['role' => $grouped]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestUser $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(RequestUser $request, User $user)
    {
        if (Auth::check())
        {
            if(Hash::check($request->password, Auth::user()->password))
            {
                $user->update(['password' => bcrypt($request->new_password), 'fn' => false]);
                $user->save();
            }
        }
        return view('index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

    }

    public function getUserData()
    {

    }
    public function loginView()
    {
        return view('user.login');
    }
    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials))
        {
            if(Auth::attempt(['username' => $request->username, 'password' => $request->password,'fn' => true])){
                return redirect('changePassword');
            }
            else
            {
                return redirect('main');
            }

            // Authentication passed...
            //return redirect()->intended('dashboard');
        }
    }
    public function changePasswordView()
    {
        return view('user.changePassword');
    }
    public function changePassword(RequestUser $request, User $user)
    {
        if (Auth::check())
        {
            if(Hash::check($request->password, Auth::user()->password))
            {
                $user->update(['password' => bcrypt($request->new_password)]);
                $user->fn = false;
                $user->save();
            }
        }
        return redirect()->action('main');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

}
