<?php

namespace App\Http\Controllers;

use App\Dependent;
use App\Employee;
use App\Http\Requests\RequestEmployee;
use App\Identification;
use App\Job;
use App\Notifications\EmployeeCreate;
use App\Notifications\UserCreate;
use App\Person;
use App\Role;
use App\Study;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use function MongoDB\BSON\toJSON;
use TaylorNetwork\UsernameGenerator\Generator;
use Yajra\DataTables\DataTables;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('employee.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobs = Job::all();
        $roles = Role::all();
        return view('employee.create',  ['jobs' => $jobs, 'roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestEmployee $request)
    {

        DB::transaction(function () use ($request) {
            $generator = new Generator();
            $username = $generator->generate($request->first_name[0].$request->last_name.$request->second_last_name[0]);
            $email_employee = $username.'@lexcorp.com';
            $person = new Person($request->all());
            $person->save();

            $employee = new Employee($request->all());
            $employee->email_employee = $email_employee;
            $employee->person()->associate($person);
            $employee->save();

            $identifications = new Identification($request->all());

                for ($i = 0; $i<count($request->type_identification); $i++)
                {
                    $identification = new Identification();
                    $identification->type_identification = $identifications->type_identification[$i];
                    $identification->number_identification = $identifications->number_identification[$i];
                    $identification->add_identification = $identifications->add_identification[$i];
                    $identification->expedition_date = $identifications->expedition_date[$i];
                    $identification->expiration_date = $identifications->expiration_date[$i];
                    $identification->expedition_place = $identifications->expedition_place[$i];

                    $identification->person()->associate($person);
                    $identification->save();
                }

            $dependents = new Dependent($request->all());
                 for ($i = 0; $i<count($request->name_dependent); $i++)
                 {
                     $dependent = new Dependent();
                     $dependent->name_dependent = $dependents->name_dependent[$i];
                     $dependent->relation_dependent = $dependents->relation_dependent[$i];
                     $dependent->birth_date_dependent = $dependents->birth_date_dependent[$i];
                     $dependent->disability_dependent = $dependents->disability_dependent[$i];

                     $dependent->person()->associate($person);
                     $dependent->save();
                 }

            $studies = new Study($request->all());

                for ($i = 0; $i<count($request->academic_level); $i++) {
                    $study = new Study();
                    $study->academic_level = $studies->academic_level[$i];
                    $study->academic_degree = $studies->academic_degree[$i];
                    $study->college = $studies->college[$i];
                    $study->date_obt = $studies->date_obt[$i];

                    $study->person()->associate($person);
                    $study->save();
                }
            $user = new User();
                $user->username = $username;
            $password = rand('1000','9999');
                $user->password = bcrypt($password);
                $user->fn = true;
                $user->person()->associate($person);

                $user->save();


            $user->roles()->attach($request->role_id);

            $userx = new User();
            $userx->username = $username;
            $userx->password = $password;


            Notification::route('mail', $person->personal_email)
                ->notify(new UserCreate($userx));

           /* Notification::route('mail', $request->personal_email)
                ->notify(new EmployeeCreate($employee));*/
        });

        return redirect()->action('EmployeeController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }

    public function getEmployeeData()
    {
        $employee = DB::table('people')
            ->join('employees', 'people.id', '=', 'employees.person_id')
            ->join('jobs', 'employees.job_id', '=', 'jobs.id')
            ->select('employees.id', 'people.first_name', 'people.last_name', 'people.second_last_name', 'employees.entry_employee', 'jobs.name_job')
            ->get();
        try {
            return Datatables::of($employee)
                ->addColumn('view', function ($employee) {
                    return view('employee.viewData', ['id' => $employee->id]);})
                ->addColumn('edit', function ($employee) {
                    return view('employee.editData', ['id' => $employee->id]);})
                ->addColumn('delete', function ($employee) {
                    return view('employee.deleteData', ['id' => $employee->id]);})
                ->rawColumns(['view', 'edit', 'delete'])->make();
        } catch (\Exception $e) {
        }
    }
}
