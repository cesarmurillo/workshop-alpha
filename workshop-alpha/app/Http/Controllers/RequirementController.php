<?php

namespace App\Http\Controllers;

use App\Job;
use App\Requirement;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class RequirementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$data = DB::table('users')
            ->join('people', 'people.id', '=', 'users.person_id')
            ->join('postulants', 'people.id', '=', 'postulants.person_id')
            ->where('users.id', '=', '3')
            ->where('postulants.requirement_id', '=', '1')
            ->select('users.id', 'postulants.requirement_id')
            ->get();

        if ($data->isEmpty())
        {
            dd('hola');
        }
        else{
            dd('noo');
        }*/

        return view('requirement.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobs = Job::all();
        return view('requirement.staffRequirement', ['jobs' => $jobs]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $requirement = new Requirement($request->all());
            $requirement->state = 'Pendiente';
            $requirement->employee()->associate($this->getID());
            $requirement->job()->associate($request->job);
            $requirement->save();
        });



        return view('requirement.allRequirements');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Requirement  $requirement
     * @return \Illuminate\Http\Response
     */
    public function show(Requirement $requirement)
    {
        $data = DB::table('requirements')
            ->join('jobs', 'requirements.job_id', '=', 'jobs.id')
            ->select('requirements.id', 'jobs.name_job',  'jobs.resume_job', 'jobs.specs_job', 'jobs.responsibility_job', 'jobs.training_job')
            ->where('requirements.id', '=', $requirement->id)
            ->get();
        return view('requirement.show', ['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Requirement  $requirement
     * @return \Illuminate\Http\Response
     */
    public function edit(Requirement $requirement)
    {
        $requirement->state = 'Aceptado';
        $requirement->save();
        return redirect()->action('RequirementController@index');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Requirement  $requirement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Requirement $requirement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Requirement  $requirement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requirement $requirement)
    {
        $requirement->state = 'Rechazado';
        $requirement->save();
        return redirect()->action('RequirementController@index');
    }

    public function getID()
    {
        $employee_id = DB::table('employees')
            ->join('people', 'employees.person_id', '=', 'people.id')
            ->join('users', 'people.id', '=', 'users.person_id')
            ->where('users.id', '=', Auth::user()->id)
            ->select('employees.id')
            ->get();
        return $employee_id[0]->id;
    }

    public function getRequirementData()
    {
        $requirement = DB::table('requirements')
            ->join('jobs', 'requirements.job_id', '=', 'jobs.id')
            ->join('employees', 'requirements.employee_id', '=', 'employees.id')
            ->select('jobs.name_job', 'requirements.state','requirements.created_at')
            ->where('employees.id', '=', $this->getID())
            ->get();
        try {
            return Datatables::of($requirement)->make();

        } catch (\Exception $e) {
        }
    }
    public function getAcceptedRequirementData()
    {
        $requirement = DB::table('requirements')
            ->join('jobs', 'requirements.job_id', '=', 'jobs.id')
            ->join('employees', 'requirements.employee_id', '=', 'employees.id')
            ->select('jobs.name_job', 'requirements.state','requirements.created_at')
            ->where('employees.id', '=', $this->getID())
            ->where('requirements.state', '=', 'Aceptado')
            ->get();
        try {
            return Datatables::of($requirement)->make();

        } catch (\Exception $e) {
        }
    }
    public function getPendingRequirementData()
    {
        $requirement = DB::table('requirements')
            ->join('jobs', 'requirements.job_id', '=', 'jobs.id')
            ->join('employees', 'requirements.employee_id', '=', 'employees.id')
            ->select('jobs.name_job', 'requirements.state','requirements.created_at')
            ->where('employees.id', '=', $this->getID())
            ->where('requirements.state', '=', 'Pendiente')
            ->get();
        try {
            return Datatables::of($requirement)->make();

        } catch (\Exception $e) {
        }
    }
    public function getRejectedRequirementData()
    {
        $requirement = DB::table('requirements')
            ->join('jobs', 'requirements.job_id', '=', 'jobs.id')
            ->join('employees', 'requirements.employee_id', '=', 'employees.id')
            ->select('jobs.name_job', 'requirements.state','requirements.created_at')
            ->where('employees.id', '=', $this->getID())
            ->where('requirements.state', '=', 'Rechazado')
            ->get();
        try {
            return Datatables::of($requirement)->make();

        } catch (\Exception $e) {
        }
    }

    public function getAllRequirementData()
    {
        $requirement = DB::table('requirements')
            ->join('jobs', 'requirements.job_id', '=', 'jobs.id')
            ->join('areas', 'jobs.area_id', '=', 'areas.id')
            ->join('employees', 'requirements.employee_id', '=', 'employees.id')
            ->select('requirements.id', 'jobs.name_job', 'areas.name_area', 'requirements.state','requirements.created_at')
            ->where('requirements.state', '=', 'Pendiente')
            ->get();
        try {
            return Datatables::of($requirement)
                ->addColumn('view', function ($requirement) {
                return view('requirement.view', ['id' => $requirement->id]);})
                ->addColumn('edit', function ($requirement) {
                    return view('requirement.accept', ['id' => $requirement->id]);})
                ->addColumn('delete', function ($requirement) {
                    return view('requirement.reject', ['id' => $requirement->id]);})
                ->rawColumns(['view', 'edit', 'delete'])->make();

        } catch (\Exception $e) {
        }
    }

    public function jobRequirement()
    {
        $requirement = DB::table('requirements')
            ->join('jobs', 'requirements.job_id', '=', 'jobs.id')
            ->join('areas', 'jobs.area_id', '=', 'areas.id')
            ->select('requirements.id', 'jobs.name_job')
            ->where('requirements.state', '=', 'Aceptado')
            ->get();
        try {
            return Datatables::of($requirement)
                ->addColumn('view', function ($requirement) {
                    return view('requirement.view', ['id' => $requirement->id]);})
                ->rawColumns(['view'])->make();

        } catch (\Exception $e) {
        }
    }


}
