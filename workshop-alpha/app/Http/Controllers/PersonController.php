<?php

namespace App\Http\Controllers;

use App\Notifications\UserCreate;
use App\Person;
use App\Privilege;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use TaylorNetwork\UsernameGenerator\Generator;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::all()->where('role' , '=', 'Invitado');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('person.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $person = new Person($request->all());
            $generator = new Generator();
            $username = $generator->generate($request->first_name[0].$request->last_name.$request->second_last_name[0]);

            $user = new User();
            $person->save();

            $user->username = $username;
            $password = rand('1000','9999');
            $user->password = bcrypt($password);
            $user->fn = true;
            $user->person()->associate($person);

            $user->save();

            $role = Role::all()->where('id' , '=', '5');
            $user->roles()->sync($role);

            $userx = new User();
            $userx->username = $username;
            $userx->password = $password;


            Notification::route('mail', $person->personal_email)
                 ->notify(new UserCreate($userx));
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function show(Person $person)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function edit(Person $person)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Person $person)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function destroy(Person $person)
    {
        //
    }
}
