<?php

namespace App\Http\Controllers;

use App\Area;
use App\Http\Requests\RequestArea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('area.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $areas = Area::all();
        return view('area.create',  ['areas' => $areas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestArea $request)
    {
        DB::transaction(function () use ($request) {
            $area = new Area($request->all());
            $area->host_create = $request->ip();
            $area->save();
        });
        return redirect()->action('AreaController@index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function show(Area $area)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function edit(Area $area)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Area $area)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy(Area $area)
    {
        //
    }
    public function getAreaData()
    {
        $area = Area::select(['id', 'name_area']);

        try {
            return Datatables::of($area)
                ->addColumn('view', function ($area) {
                    return view('area.viewData', ['id' => $area->id]);})
                ->addColumn('edit', function ($area) {
                    return view('area.editData', ['id' => $area->id]);})
                ->addColumn('delete', function ($area) {
                    return view('area.deleteData', ['id' => $area->id]);})
                ->rawColumns(['view', 'edit', 'delete'])->make();
        } catch (\Exception $e) {
        }
    }
}
