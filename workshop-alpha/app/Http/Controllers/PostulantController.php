<?php

namespace App\Http\Controllers;

use App\Postulant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\New_;

class PostulantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            if ($request->hasFile('cv')) {
                $path = $request->file('cv')->store('public/storage');
                $postulant = New Postulant();
                $postulant->size = '300';
                $postulant->name = $path;
                $postulant->person_id = $this->getPersonId();
                $postulant->requirement_id = $request->requirement_id;
                $postulant->save();
            }
        });

    }

    public function getPersonId()
    {
        $id = DB::table('people')
            ->join('users', 'people.id', '=', 'users.person_id')
            ->where('users.id', '=', Auth::user()->id)
            ->select('people.id')
            ->get();
        return $id[0]->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Postulant  $postulant
     * @return \Illuminate\Http\Response
     */
    public function show(Postulant $postulant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Postulant  $postulant
     * @return \Illuminate\Http\Response
     */
    public function edit(Postulant $postulant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Postulant  $postulant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Postulant $postulant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Postulant  $postulant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Postulant $postulant)
    {
        //
    }
}
