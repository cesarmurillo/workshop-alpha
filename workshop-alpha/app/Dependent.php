<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dependent extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected  $fillable = [
        'name_dependent',
        'relation_dependent',
        'birth_date_dependent',
        'disability_dependent',
        'person_id',
        'user_create',
        'user_update',
        'user_delete',
        'host_create',
        'host_update',
        'host_delete',
    ];

    public function person()
    {
        return $this->belongsTo('App\Person');
    }
}
