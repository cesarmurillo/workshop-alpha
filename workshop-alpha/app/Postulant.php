<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Postulant extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'size',
        'person_id',
        'requirement_id',
        'user_create',
        'user_update',
        'user_delete',
        'host_create',
        'host_update',
        'host_delete',
    ];
    public function person()
    {
        return $this->belongsTo('App\Person');
    }
    public function requirement()
    {
        return $this->belongsTo('App\Requirement');
    }
}
