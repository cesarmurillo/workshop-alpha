<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\DB;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewStaffRequirement(User $user)
    {
        if($this->getRoleID($user->id) == 3)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function viewStaffRequest(User $user)
    {
        if($this->getRoleID($user->id) == 2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function viewJobRequirement(User $user)
    {
        if($this->getRoleID($user->id) == 5)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getRoleID($id)
    {
        $role = DB::table('roles')
            ->join('role_user', 'roles.id', '=', 'role_user.role_id')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->where('users.id', '=', $id)
            ->select('roles.id')
            ->whereNull('roles.deleted_at')
            ->get();
        return $role[0]->id;
    }

    public function apply(User $user, $requirementId)
    {
        $data = DB::table('users')
            ->join('people', 'people.id', '=', 'users.person_id')
            ->join('postulants', 'people.id', '=', 'postulants.person_id')
            ->where('users.id', '=', $user->id)
            ->where('postulants.requirement_id', '=', $requirementId)
            ->select('users.id', 'postulants.requirement_id')
            ->get();
        if ($data->isEmpty())
        {
            return true;
        }
        else{
            return false;
        }
    }

}
