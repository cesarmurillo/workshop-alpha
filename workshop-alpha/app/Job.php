<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected  $fillable = ['name_job',
                            'resume_job',
                            'specs_job',
                            'condition_job',
                            'duty_job',
                            'responsibility_job',
                            'training_job',
                            'area_id',
                            'user_create',
                            'user_update',
                            'user_delete',
                            'host_create',
                            'host_update',
                            'host_delete',
                            ];

    public function area()
    {
        return $this->belongsTo('App\Area');
    }

    public function requirements()
    {
        return $this->hasMany('App\Requirement');
    }

}
