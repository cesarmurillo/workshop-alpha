<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Identification extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected  $fillable = [
        'type_identification',
        'number_identification',
        'add_identification',
        'expedition_place',
        'expedition_date',
        'expiration_date',
        'person_id',
        'user_create',
        'user_update',
        'user_delete',
        'host_create',
        'host_update',
        'host_delete',
    ];

    public function person()
    {
        return $this->belongsTo('App\Person');
    }

}
