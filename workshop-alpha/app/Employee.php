<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{

    protected  $fillable = [
                            'email_employee',
                            'entry_employee',
                            'salary_employee',
                            'labor_union',
                            'job_id',
                            'person_id',
                            'user_create',
                            'user_update',
                            'user_delete',
                            'host_create',
                            'host_update',
                            'host_delete',
                            ];

    public function person()
    {
        return $this->belongsTo('App\Person');
    }

    public function requirements()
    {
        return $this->hasMany('App\Requirement');
    }
}
