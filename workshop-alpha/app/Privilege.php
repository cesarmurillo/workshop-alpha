<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Privilege extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'privilege',
        'user_create',
        'user_update',
        'user_delete',
        'host_create',
        'host_update',
        'host_delete',
    ];

    protected $dates = ['deleted_at'];

    public function roles()
    {
        return $this->belongsToMany('App\Role')->withPivot('user_create',
                                                                    'user_update',
                                                                    'user_delete',
                                                                    'host_create',
                                                                    'host_update',
                                                                    'host_delete')->withTimestamps();
    }
}
