<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected  $fillable = ['name_area',
        'description_area',
        'area_id',
        'area_id',
        'user_create',
        'user_update',
        'user_delete',
        'host_create',
        'host_update',
        'host_delete'];

    public function jobs()
    {
        return $this->hasMany('App\Job');
    }
}
