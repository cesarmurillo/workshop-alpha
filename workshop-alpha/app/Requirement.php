<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Requirement extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'justification',
        'state',
        'job_id',
        'employee_id',
        'user_create',
        'user_update',
        'user_delete',
        'host_create',
        'host_update',
        'host_delete',
    ];

    protected $dates = ['deleted_at'];

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

    public function job()
    {
        return $this->belongsTo('App\Job');
    }
    public function postulants()
    {
        return $this->hasMany('App\Postulant');
    }



}
