<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Study extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected  $fillable = [
        'academic_degree',
        'academic_level',
        'college',
        'date_obt',
        'person_id',
        'user_create',
        'user_update',
        'user_delete',
        'host_create',
        'host_update',
        'host_delete',
    ];

    public function person()
    {
        return $this->belongsTo('App\Person');
    }
}
