<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'role',
        'user_create',
        'user_update',
        'user_delete',
        'host_create',
        'host_update',
        'host_delete',
    ];

    protected $dates = ['deleted_at'];

    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('user_create',
            'user_update',
            'user_delete',
            'host_create',
            'host_update',
            'host_delete')->withTimestamps();
    }
    public function privileges()
    {
        return $this->belongsToMany('App\Privilege')->withPivot('user_create',
                                                                        'user_update',
                                                                        'user_delete',
                                                                        'host_create',
                                                                        'host_update',
                                                                        'host_delete')->withTimestamps();
    }
}
