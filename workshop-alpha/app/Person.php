<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected  $fillable = ['first_name', 'last_name', 'second_last_name',
                            'gender', 'birth_date', 'personal_email', 'phone_number',
                            'cell_number', 'address', 'birth_place', 'nacionality', 'civil_status',
                            'user_create',
                            'user_update',
                            'user_delete',
                            'host_create',
                            'host_update',
                            'host_delete',];

    public function employee()
    {
        return $this->hasOne('App\Employee');
    }
    public function user()
    {
        return $this->hasOne('App\User');
    }
    public function identifications()
    {
        return $this->hasMany('App\Identification');
    }
    public function postulants()
    {
        return $this->hasMany('App\Postulant');
    }
    public function studies()
    {
        return $this->hasMany('App\Study');
    }
    public function dependents()
    {
        return $this->hasMany('App\Dependent');
    }
}
