@extends('template.main')

@section('title', 'Cambiar Contraseña')
@section('section', 'Gestion de cuenta')
@section('content')
    {!! Form::open([ 'method'  => 'put', 'route' => [ 'user.update', Auth::user()->id ], 'class' => 'horizontal-form' ]) !!}

    <div class="form-group">
        <label for="new_password">Ingrese su contraseña</label>
        <input type="password" class="form-control" name="password"
               aria-describedby="password_help" placeholder="">
    </div>
    <div class="form-group">
        <label for="new_password">Nueva contraseña</label>
        <input type="password" class="form-control" name="new_password"
               aria-describedby="privilege_password-help" placeholder="">
    </div>
    <div class="form-group">
        <label for="new_password">Repita su nueva contraseña</label>
        <input type="password" class="form-control" name="new_password"
               aria-describedby="new_password-help" placeholder="">
    </div>
    <button type="submit" class="btn btn-primary">Cambiar contraseña</button>
    {!! Form::close() !!}
@endsection

