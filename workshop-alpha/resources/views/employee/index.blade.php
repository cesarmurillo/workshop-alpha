@extends('template.main')

@section('title', 'Empleados')
@section('content')
    <div class="table-responsive">
        <table class="table" id="employee-table">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Apellido paterno</th>
                <th>Apellido materno</th>
                <th>Fecha de ingreso</th>
                <th>Cargo</th>
                <th style="width: 67px; text-align:center">Ver</th>
                <th style="width: 67px; text-align:center">Editar</th>
                <th style="width: 67px; text-align:center">Eliminar</th>
            </tr>
            </thead>

        </table>
    </div>

@endsection
@push('scripts')
    <script>
        $(function() {
            $('#employee-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('EmployeeData') !!}',
                columns: [
                    { data: 'first_name', name: 'first_name' },
                    { data: 'last_name', name: 'last_name' },
                    { data: 'second_last_name', name: 'second_last_name' },
                    { data: 'entry_employee', name: 'entry_employee' },
                    { data: 'name_job', name: 'name_job' },
                    { data: 'view', name: 'view', orderable: false, searchable: false},
                    { data: 'edit', name: 'edit', orderable: false, searchable: false},
                    { data: 'delete', name: 'delete', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endpush