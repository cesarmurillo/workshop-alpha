@extends('template.main')

@section('title', 'Registro de Empleados')
@section('content')
    {!! Form::open(['url' => 'employee', 'id' => 'employee-form']) !!}
    <div class="card-title">
        <h4>Datos Personales</h4>
    </div>
    <hr class="m-t-0 m-b-40">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <div class="col-md-9">
                    <input type="text" class="form-control " id="first_name" name="first_name" aria-describedby="first_name-help" placeholder="Nombres del Empleado" autocomplete="off">
                    <small id="first_name-help" class="form-text text-muted">Nombres.</small>
                </div>
            </div>
        </div>
        <!--/span-->
        <div class="col-md-6">
            <div class="form-group has-danger row">
                <div class="col-md-9">
                    <input type="text" class="form-control " id="last_name" name="last_name" aria-describedby="last_name-help" placeholder="Apellido Paterno" autocomplete="off">
                    <small id="last_name-help" class="form-text text-muted">Apellido Paterno.</small>
                    <input type="text" class="form-control" id="second_last_name" name="second_last_name" aria-describedby="second_last_name-help" placeholder="Apellido Materno" autocomplete="off">
                    <small id="second_last_name-help" class="form-text text-muted">Apellido Materno.</small>
                </div>
            </div>
        </div>
        <!--/span-->
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <div class="col-md-9">
                    <div class="form-group">
                        <select class="form-control" id="gender" name="gender" aria-required="true" aria-describedby="" aria-invalid="">
                            <option value="" disabled selected>Género</option>
                            <option value="Masculino">Masculino</option>
                            <option value="Femenino">Femenino</option>
                        </select>
                        <small id="gender-help" class="form-text text-muted " >Género de la persona.</small>
                    </div>
                </div>
            </div>
        </div>
        <!--/span-->
        <div class="col-md-6">
            <div class="form-group has-danger row">
                <div class="col-md-9">
                    <div class="form-group">
                        <select class="form-control " id="civil_status" name="civil_status" aria-required="true" aria-describedby="" aria-invalid="">
                            <option value="" disabled selected>Estado Civil</option>
                            <option value="Soltero(a)">Soltero(a)</option>
                            <option value="Casado(a)">Soltero(a)</option>
                            <option value="Divorsiado(a)">Divorsiado(a)</option>
                            <option value="Viudo(a)">Viudo(a)</option>
                        </select>
                        <small id="civil_status-help" class="form-text text-muted " >Estado civil de la persona.</small>
                    </div>
                </div>
            </div>
        </div>
        <!--/span-->
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <div class="col-md-9">
                    <div class="form-group">
                        <input type="text" class="form-control" id="address" name="address" aria-describedby="address-help" placeholder="Dirección" autocomplete="off">
                        <small id="address-help" class="form-text text-muted">Dirección.</small>
                    </div>
                </div>
            </div>
        </div>
        <!--/span-->
        <div class="col-md-6">
            <div class="form-group has-danger row">
                <div class="col-md-9">
                    <div class="form-group">
                        <input type="number" class="form-control" id="phone_number" name="phone_number" aria-describedby="phone_number-help" placeholder="Número Telefonico" autocomplete="off">
                        <small id="phone_number-help" class="form-text text-muted">Número Telefonico.</small>
                    </div>
                </div>
            </div>
        </div>
        <!--/span-->
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <div class="col-md-9">
                    <div class="form-group">
                        <input type="email" class="form-control" id="personal_email" name="personal_email" aria-describedby="second_last_name-help" placeholder="Email Personal" autocomplete="off">
                        <small id="personal_email-help" class="form-text text-muted">Email.</small>
                    </div>

                </div>
            </div>
        </div>
        <!--/span-->
        <div class="col-md-6">
            <div class="form-group has-danger row">
                <div class="col-md-9">
                    <div class="form-group">
                        <input type="number" class="form-control" id="cell_number" name="cell_number" aria-describedby="cell_number-help" placeholder="Número de Celular" autocomplete="off">
                        <small id="cell_number-help" class="form-text text-muted">Número de Celular.</small>
                    </div>
                </div>
            </div>
        </div>
        <!--/span-->
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <div class="col-md-9">
                    <div class="form-group">
                        <input type="date" class="form-control " id="birth_date" name="birth_date" placeholder="dd/mm/yyyy">
                        <small id="cell_number-help" class="form-text text-muted">Fecha de nacimiento.</small>
                    </div>
                    <input type="text" class="form-control" id="nacionality" name="nacionality" aria-describedby="cell_number-help" placeholder="Nacionalidad" autocomplete="off">
                    <small id="nacionality-help" class="form-text text-muted">Nacionalidad.</small>
                </div>
            </div>
        </div>
        <!--/span-->
        <div class="col-md-6">
            <div class="form-group has-danger row">
                <div class="col-md-9">
                    <div class="form-group">
                        <input type="text" class="form-control" id="birth_place" name="birth_place" aria-describedby="cell_number-help" placeholder="Lugar de nacimiento." autocomplete="off">
                        <small id="birth_place-help" class="form-text text-muted">Lugar de nacimiento.</small>
                    </div>
                </div>
            </div>
        </div>
        <!--/span-->
    </div>

    <div class="form-group">

        <label for="">Identificacion:</label>
        <button type="button" class="btn btn-info btn-flat btn-addon btn-sm m-b-10 m-l-5" id="btn1"><i class="ti-plus"></i>Añadir</button>

        <div class=""></div>
        <div id="xd">

        </div>

    </div>
    <div class="form-group">

        <label for="">Dependientes:</label>
        <button type="button" class="btn btn-info btn-flat btn-addon btn-sm m-b-10 m-l-5" id="btnb"><i class="ti-plus"></i>Añadir</button>

        <div class=""></div>
        <div id="xf">

        </div>

    </div>
    <div class="form-group">

        <label for="">Estudios academicos:</label>
        <button type="button" class="btn btn-info btn-flat btn-addon btn-sm m-b-10 m-l-5" id="btna"><i class="ti-plus"></i>Añadir</button>

        <div class=""></div>
        <div id="xe">

        </div>

    </div>

    <div class="card-title">
        <h4>Datos de Empleado</h4>
    </div>
    <hr class="m-t-0 m-b-40">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <div class="col-md-9">
                    <div class="form-group">
                        <input type="date" class="form-control" id="entry_employee" name="entry_employee" placeholder="dd/mm/yyyy">
                        <small id="cell_number-help" class="form-text text-muted">Fecha de entrada.</small>
                    </div>
                </div>
            </div>
        </div>
        <!--/span-->
        <div class="col-md-6">
            <div class="form-group has-danger row">
                <div class="col-md-9">
                    <div class="form-group">
                        <input type="number" class="form-control" min="1" step="any" id="salary_employee" name="salary_employee" aria-describedby="cell_number-help" placeholder="Salario" autocomplete="off">
                        <small id="nacionality-help" class="form-text text-muted">Salario.</small>
                    </div>
                </div>
            </div>
        </div>
        <!--/span-->
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <div class="col-md-9">
                    <div class="form-group">
                        <select class="form-control valid " id="job_id" name="job_id" aria-required="true" aria-describedby="" aria-invalid="">
                            <option value="" disabled selected>Puesto de trabajo</option>
                            @foreach($jobs as $job)
                                <option value="{{ $job->id }}">{{ $job->name_job }}</option>
                            @endforeach
                        </select>
                        <small id="area-help" class="form-text text-muted " >El cargo o puesto que desempeñara el empleado.</small>
                    </div>
                </div>
            </div>
        </div>
        <!--/span-->
        <div class="col-md-6">
            <div class="form-group has-danger row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label>
                            <input type="checkbox" class="" id="labor_union" name="labor_union" value="true">
                            Sindicalizado
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <!--/span-->
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group row">
                <div class="col-md-9">
                    <div class="form-group">
                        <select class="form-control valid " id="job_id" name="role_id" aria-required="true" aria-describedby="" aria-invalid="">
                            <option value="" disabled selected>Rol</option>
                            @foreach($roles as $role)
                                <option value="{{$role->id}}">{{ $role->role }}</option>
                            @endforeach
                        </select>
                        <small id="area-help" class="form-text text-muted ">El rol que asumira en el sistema.</small>
                    </div>
                </div>
            </div>
        </div>
        <!--/span-->
    </div>

    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i>Registrar</button>
    {!! Form::close() !!}
@endsection

@push('scripts')
    <script src="{{ asset('js/employeeForm.js') }}"></script>
    <script>
        var aa=0;
        var bb=0;
        var cc=0;
        $("#btn1").click(function(){

            $("#xd").append("<div>\n" +
                "\n" +
                "    <div class=\"row\">\n" +
                "        <div class=\"col-md-5\">\n" +
                "            <div class=\"form-group row\">\n" +
                "                <div class=\"col-md-9\">\n" +
                "                    <div class=\"form-group\">\n" +
                "                        <input type=\"text\" class=\"form-control\" min=\"1\" step=\"any\" id=\"type_identification\" name=\"type_identification[]\" aria-describedby=\"type_identification-help\" placeholder=\"Tipo de identificación\" autocomplete=\"off\">\n" +
                "                        <small id=\"type_identification-help\" class=\"form-text text-muted\">Tipo de identificacion (CI, Pasaporte).</small>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "        <div class=\"col-md-5\">\n" +
                "            <div class=\"form-group has-danger row\">\n" +
                "                <div class=\"col-md-9\">\n" +
                "                    <div class=\"form-group\">\n" +
                "                        <input type=\"number\" class=\"form-control\" min=\"1\" id=\"number_identification\" name=\"number_identification[]\" aria-describedby=\"number_identification-help\" placeholder=\"Número de identificación\" autocomplete=\"off\">\n" +
                "                        <small id=\"number_identification-help\" class=\"form-text text-muted\">Número de serie del documento de identificación.</small>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "        <div class=\"col-md-2\">\n" +
                "            <div class=\"form-group has-danger row\">\n" +
                "                <div class=\"col-md-9\">\n" +
                "                    <div class=\"form-group\">\n" +
                "                        <input type=\"text\" class=\"form-control\" id=\"add_identification\" name=\"add_identification[]\" aria-describedby=\"add_identification-help\" placeholder=\"Add\" autocomplete=\"off\">\n" +
                "                        <small id=\"number_identification-help\" class=\"form-text text-muted\">Add.</small>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "    <div class=\"row\">\n" +
                "        <div class=\"col-md-6\">\n" +
                "            <div class=\"form-group row\">\n" +
                "                <div class=\"col-md-9\">\n" +
                "                    <div class=\"form-group\">\n" +
                "                        <input type=\"text\" class=\"form-control\" id=\"expedition_place\" name=\"expedition_place[]\" aria-describedby=\"expedition_place-help\" placeholder=\"Lugar de emisión\" autocomplete=\"off\">\n" +
                "                        <small id=\"expedition_place-help\" class=\"form-text text-muted\">Expedido en.</small>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "        <div class=\"col-md-6\">\n" +
                "            <div class=\"form-group has-danger row\">\n" +
                "                <div class=\"col-md-9\">\n" +
                "                    <div class=\"form-group\">\n" +
                "                        <input type=\"date\" class=\"form-control\" id=\"expedition_date\" name=\"expedition_date[]\" placeholder=\"dd/mm/yyyy\">\n" +
                "                        <small id=\"expedition_date-help\" class=\"form-text text-muted\">Fecha de emisión.</small>\n" +
                "                        <input type=\"date\" class=\"form-control\" id=\"expiration_date\" name=\"expiration_date[]\" placeholder=\"dd/mm/yyyy\">\n" +
                "                        <small id=\"expiration_date-help\" class=\"form-text text-muted\">Fecha de expiración.</small>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "        \n" +
                "    </div>\n" +
                "        <button id='btn2' data-id='\"+aa+\"' type=\"button\" class=\"btn btn-danger btn-flat btn-addon btn-sm m-b-10 m-l-5\"><i class=\"ti-close\"></i></button>\n" +
                "    </div>")
            aa++;

        });
        $('#xd').on('click','#btn2',function() {
            $(this).parent().remove();
        });

        $("#btna").click(function(){

            $("#xe").append("<div>\n" +
                "        <div class=\"row\">\n" +
                "            <div class=\"col-md-6\">\n" +
                "                <div class=\"form-group row\">\n" +
                "                    <div class=\"col-md-9\">\n" +
                "                        <div class=\"form-group\">\n" +
                "                            <input type=\"text\" class=\"form-control \" id=\"academic_level\" name=\"academic_level[]\" aria-describedby=\"academic_level-help\" placeholder=\"Grado academico\" autocomplete=\"off\">\n" +
                "                            <small id=\"academic_level-help\" class=\"form-text text-muted\">Grado academico</small>\n" +
                "                            <input type=\"text\" class=\"form-control \" id=\"college\" name=\"college[]\" aria-describedby=\"college-help\" placeholder=\"Establecimiento o instituto academico\" autocomplete=\"off\">\n" +
                "                            <small id=\"college-help\" class=\"form-text text-muted\">Establecimiento o instituto academico</small>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "            <div class=\"col-md-6\">\n" +
                "                <div class=\"form-group has-danger row\">\n" +
                "                    <div class=\"col-md-9\">\n" +
                "                        <div class=\"form-group\">\n" +
                "                            <input type=\"text\" class=\"form-control \" id=\"academic_degree\" name=\"academic_degree[]\" aria-describedby=\"academic_degree-help\" placeholder=\"Titulo academico\" autocomplete=\"off\">\n" +
                "                            <small id=\"academic_degree-help\" class=\"form-text text-muted\">Titulo academico en:</small>\n" +
                "                            <input type=\"date\" class=\"form-control\" id=\"date_obt\" name=\"date_obt[]\" placeholder=\"dd/mm/yyyy\">\n" +
                "                            <small id=\"date_obt-help\" class=\"form-text text-muted\">Fecha de obtención.</small>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "        <button id='btnaa' data-id='\"+bb+\"' type=\"button\" class=\"btn btn-danger btn-flat btn-addon btn-sm m-b-10 m-l-5\"><i class=\"ti-close\"></i></button>\n" +
                "    </div>")
            bb++;

        });
        $('#xe').on('click','#btnaa',function() {
            $(this).parent().remove();
        });

        $("#btnb").click(function(){

            $("#xf").append("<div>\n" +
                "        <div class=\"row\">\n" +
                "            <div class=\"col-md-6\">\n" +
                "                <div class=\"form-group row\">\n" +
                "                    <div class=\"col-md-9\">\n" +
                "                        <div class=\"form-group\">\n" +
                "                            <input type=\"text\" class=\"form-control\" id=\"name_dependent\" name=\"name_dependent[]\" aria-describedby=\"name_dependent-help\" placeholder=\"Nombre completo\" autocomplete=\"off\">\n" +
                "                            <small id=\"name_dependent-help\" class=\"form-text text-muted\">Nombre completo del dependiente.</small>\n" +
                "                            <input type=\"text\" class=\"form-control\" id=\"relation_dependent\" name=\"relation_dependent[]\" aria-describedby=\"relation_dependent-help\" placeholder=\"Relacion o parentesco\" autocomplete=\"off\">\n" +
                "                            <small id=\"relation_dependent-help\" class=\"form-text text-muted\">Tipo de relacion o parentesco.</small>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "\n" +
                "            <div class=\"col-md-6\">\n" +
                "                <div class=\"form-group has-danger row\">\n" +
                "                    <div class=\"col-md-9\">\n" +
                "                        <div class=\"form-group\">\n" +
                "                            <input type=\"date\" class=\"form-control\" id=\"birth_date_dependent\" name=\"birth_date_dependent[]\" placeholder=\"dd/mm/yyyy\">\n" +
                "                            <small id=\"cell_number-help\" class=\"form-text text-muted\">Fecha de nacimiento</small>\n" +
                "                            <input type=\"text\" class=\"form-control\" id=\"disability_dependent\" name=\"disability_dependent[]\" aria-describedby=\"disability_dependent-help\" placeholder=\"Discapacidad\" autocomplete=\"off\">\n" +
                "                            <small id=\"disability_dependent-help\" class=\"form-text text-muted\">Presenta algun tipo de discapacidad.</small>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>\n" +
                "\n" +
                "        </div>\n" +
                "        <button id='btnbb' data-id='\"+cc+\"' type=\"button\" class=\"btn btn-danger btn-flat btn-addon btn-sm m-b-10 m-l-5\"><i class=\"ti-close\"></i></button>\n" +
                "    </div>")
            cc++;

        });
        $('#xf').on('click','#btnbb',function() {
            $(this).parent().remove();
        });
    </script>
@endpush