@extends('template.main')

@section('title', 'Modificación de Privilegios')

@section('content')
    {!! Form::open([ 'method'  => 'put', 'route' => [ 'privilege.update', $privilege->id ] ]) !!}
    <div class="form-group">
        <label for="privilege">Privilegio</label>
        <input type="text" class="form-control" name="privilege"
               aria-describedby="privilege_help" placeholder="Ingrese un privilegio"
               value="{{ $privilege->privilege }}">
        <small id="activity_help" class="form-text text-muted">Las opciones o funciones que se encontraran disponibles.</small>
    </div>
    <button type="submit" class="btn btn-primary">Actualizar</button>
    {!! Form::close() !!}

@endsection