@extends('template.main')

@section('title', 'Privilegios')
@section('content')
    <div class="table-responsive">
        <table class="table" id="privilege-table">
            <thead>
            <tr>
                <th>Name</th>
                <th style="width: 67px; text-align:center">Ver</th>
                <th style="width: 67px; text-align:center">Editar</th>
                <th style="width: 67px; text-align:center">Eliminar</th>
            </tr>
            </thead>

        </table>
    </div>

@endsection
@push('scripts')
    <script>
        $(function() {
            $('#privilege-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('PrivilegeData') !!}',
                columns: [
                    { data: 'privilege', name: 'privilege' },
                    { data: 'view', name: 'view', orderable: false, searchable: false},
                    { data: 'edit', name: 'edit', orderable: false, searchable: false},
                    { data: 'delete', name: 'delete', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endpush