@extends('template.main')

@section('title', 'Creación de Privilegios')
@section('content')
    {!! Form::open(['url' => 'privilege', 'id' => 'privilege-form']) !!}
    <div class="form-group">
        <label for="privilege">Privilegio</label>
        <input type="text" class="form-control" id="privilege" name="privilege" aria-describedby="privilege-help" placeholder="Ingrese un privilegio" autocomplete="off">
        <small id="privilege-help" class="form-text text-muted">Las opciones o funciones que se encontraran disponibles.</small>
    </div>
    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i>Registrar</button>
    {!! Form::close() !!}
    <div id="output2"></div>
@endsection
@push('scripts')
    <script src="{{ asset('js/privilegeForm.js') }}"></script>
@endpush