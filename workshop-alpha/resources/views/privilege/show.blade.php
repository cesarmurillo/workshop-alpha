@extends('template.main')

@section('title', 'Ver Privilegios')

@section('content')

    <div class="form-group form-inline">
        <label class="black">Privilegio:  </label>
        <label class="form-control-static">{{ $privilege->privilege }}</label>
    </div>
    <div class="form-group form-inline">
        <label>Creado por:</label>
        <label class="form-control-static">{{ $privilege->user_create }}</label>

        <label>en fecha:</label>
        <label class="form-control-static">{{ $privilege->created_at }}</label>
    </div>
    <div class="form-group form-inline">
        <label for="privilege">Modificado por:</label>
        <input class="form-control" type="text" value="{{ $privilege->user_update }}" readonly="">
        <label for="privilege">en fecha:</label>
        <input class="form-control" type="text" value="{{ $privilege->updated_at }}" readonly="">
    </div>
    <div class="form-group form-inline">
        <label for="privilege">Eliminado por:</label>
        <input class="form-control" type="text" value="{{ $privilege->user_delete }}" readonly="">
        <label for="privilege">en fecha:</label>
        <input class="form-control" type="text" value="{{ $privilege->deleted_at }}" readonly="">
    </div>

@endsection

