@extends('template.main')

@section('title', 'Areas de la empresa')
@section('content')
    <div class="table-responsive">
        <table class="table" id="area-table">
            <thead>
            <tr>
                <th>Area</th>
                <th style="width: 67px; text-align:center">Ver</th>
                <th style="width: 67px; text-align:center">Editar</th>
                <th style="width: 67px; text-align:center">Eliminar</th>
            </tr>
            </thead>

        </table>
    </div>

@endsection
@push('scripts')
    <script>
        $(function() {
            $('#area-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('AreaData') !!}',
                columns: [
                    { data: 'name_area', name: 'name_area' },
                    { data: 'view', name: 'view', orderable: false, searchable: false},
                    { data: 'edit', name: 'edit', orderable: false, searchable: false},
                    { data: 'delete', name: 'delete', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endpush