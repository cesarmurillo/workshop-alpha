@extends('template.main')

@section('title', 'Creación de Areas de la Empresa')
@section('content')
    {!! Form::open(['url' => 'area', 'id' => 'area-form']) !!}
    <div class="form-group">
        <label for="area">Area</label>
        <input type="text" class="form-control" id="area" name="name_area" aria-describedby="area-help" placeholder="Ingrese una Area" autocomplete="off">
        <small id="privilege-help" class="form-text text-muted">Las areas que existen dentro de la empresa.</small>
    </div>
    <div class="form-group">
        <label for="area_description">Descripción</label>
        <textarea type="text" class="form-control" id="area_description" name="description_area" aria-describedby="area_description-help" placeholder="Descripcion del area" autocomplete="off"></textarea>
        <small id="privilege-help" class="form-text text-muted">Descripcion general del area a registrar.</small>
    </div>
    <div class="form-group">
        <label for="area_id">Dependiente de:</label>
        <select class="form-control valid" id="area_id" name="area_id" aria-required="true" aria-describedby="" aria-invalid="">
            <option value="" disabled selected>Seleccione uno</option>
                @foreach($areas as $area)
                    <option value="{{ $area->id }}">{{ $area->name_area }}</option>
                @endforeach
        </select>
        <small id="area-help" class="form-text text-muted">El area del cual depende.</small>
    </div>

    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i>Registrar</button>
    {!! Form::close() !!}
@endsection
