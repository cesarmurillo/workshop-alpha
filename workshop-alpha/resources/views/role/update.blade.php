@extends('template.main')

@section('title', 'Creación de Roles')
@section('content')
    {!! Form::open([ 'method'  => 'put', 'route' => [ 'role.update', $role->id ] ]) !!}
    <div class="form-group ">
        <label for="role">Rol</label>
        <input type="text" class="form-control" id="role" name="role" aria-describedby="role-help" placeholder="Ingrese un rol"
               value="{{ $role->role }}"
               autocomplete="off">
        <small id="role-help" class="form-text text-muted">Los roles que tendran los usuarios</small>
    </div>
    <div class="form-group">
        <label>Seleccione los privilegios</label>
        <select style="height: 80%;" name="privilege[]" multiple="multiple" class="form-control">
            @foreach($privileges as $privilege)
                <option value="{{ $privilege->id }}">{{ $privilege->privilege }}</option>
            @endforeach
        </select>
        <small id="privilege-help" class="form-text text-muted">Los los privilegios del sistema que gozara el rol</small>
    </div>
    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i>Actualizar</button>
    {!! Form::close() !!}
@endsection
