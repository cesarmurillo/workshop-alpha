@extends('template.main')

@section('title', 'Roles')
@section('content')
    <div class="table-responsive">
        <table class="table" id="role-table">
            <thead>
            <tr>
                <th>Rol</th>
                <th style="width: 67px; text-align:center">Ver</th>
                <th style="width: 67px; text-align:center">Editar</th>
                <th style="width: 67px; text-align:center">Eliminar</th>
            </tr>
            </thead>

        </table>
    </div>

@endsection
@push('scripts')
    <script>
        $(function() {
            $('#role-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('RoleData') !!}',
                columns: [
                    { data: 'role', name: 'role' },
                    { data: 'view', name: 'view', orderable: false, searchable: false},
                    { data: 'edit', name: 'edit', orderable: false, searchable: false},
                    { data: 'delete', name: 'delete', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endpush