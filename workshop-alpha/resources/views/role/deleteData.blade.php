{!! Form::open([ 'method'  => 'delete', 'route' => [ 'role.destroy', $id ] ]) !!}
<center>
    <button type="submit" class="btn btn-danger m-b-10 m-l-5"><i class="fa fa-trash"></i></button>
</center>
{!! Form::close() !!}