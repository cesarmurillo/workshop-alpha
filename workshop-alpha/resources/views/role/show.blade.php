@extends('template.main')

@section('title', 'Creación de Roles')
@section('content')

    <div class="form-group ">
        <label for="role">Rol:</label>
        <label>{{ $role->role }}</label>
    </div>
    <div class="form-group">
        <label>Privilegios</label>
            @foreach($privileges as $privilege)
                <li><label>{{ $privilege->privilege }}</label></li>
            @endforeach
    </div>

@endsection
