@extends('template.main')

@section('title', 'Bolsa de Trabajo')
@section('section', 'Recursos Humanos')
@section('content')
    <div class="table-responsive">
        <table class="table" id="requirements-table">
            <thead>
            <tr>
                <th>Puesto</th>
                <th style="width: 67px; text-align:center">Ver</th>
            </tr>
            </thead>

        </table>
    </div>

@endsection
@push('scripts')
    <script>
        $(function() {
            $('#requirements-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('JobRequirementData') !!}',
                columns: [
                    { data: 'name_job', name: 'name_job' },
                    { data: 'view', name: 'view', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endpush