@extends('template.main')

@section('title', 'Creación de Privilegios')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-content">
                        <!-- Left sidebar -->
                        <div class="inbox-leftbar">

                            <a href="email-inbox.html" class="btn btn-danger btn-block waves-effect waves-light">Nueva solicitud</a>

                            <div class="mail-list mt-4">
                                <a href="#" class="list-group-item border-0"><i class="mdi mdi-inbox font-18 align-middle mr-2"></i><b>Pendientes</b></a>
                                <a href="#" class="list-group-item border-0"><i class="mdi mdi-star font-18 align-middle mr-2"></i><b>Aceptadas </b></a>
                                <a href="#" class="list-group-item border-0"><i class="mdi mdi-file-document-box font-18 align-middle mr-2"></i><b>Rechazadas</b></a>
                                <a href="#" class="list-group-item border-0"><i class="mdi mdi-send font-18 align-middle mr-2"></i><b>Todas</b></a>
                            </div>

                        </div>
                        <!-- End Left sidebar -->
                        <div class="inbox-rightbar">

                            <div class="mt-4" id="fill">

                                {!! Form::open(['url' => 'requirement', 'id' => 'requirement-form']) !!}
                                <div class="form-group">
                                <select style="height: 80%;" name="job" class="form-control">
                                    <option value="" disabled selected>Puesto de trabajo</option>
                                    @foreach($jobs as $job)
                                        <option value="{{ $job->id }}">{{ $job->name_job }}</option>
                                    @endforeach
                                </select>
                                <small id="job-help" class="form-text text-muted">El puesto de trabajo a solicitar</small>
                                </div>
                                <div class="form-group">
                                    <textarea name="justification" rows="8" cols="80" class="form-control" style="height:300px"></textarea>
                                    <small id="justification-help" class="form-text text-muted">Justifiación del requerimiento</small>
                                </div>
                                <div class="text-right">
                                <button class="btn btn-purple waves-effect waves-light" type="submit"> <span>Send</span> <i class="fa fa-send m-l-10"></i> </button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                            <!-- end card-->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('js/requirementForm.js') }}"></script>
@endpush