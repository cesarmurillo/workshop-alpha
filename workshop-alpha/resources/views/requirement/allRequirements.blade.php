@extends('template.main')

@section('title', 'Requerimeinto de personal')
@section('section', 'Recursos Humanos')
@section('content')
    <div class="table-responsive">
        <table class="table" id="requirements-table">
            <thead>
            <tr>
                <th>Puesto</th>
                <th>Area</th>
                <th>Estado</th>
                <th>Fecha</th>
                <th>Ver</th>
                <th>Aceptar</th>
                <th>rechazar</th>
            </tr>
            </thead>

        </table>
    </div>
@endsection
@push('scripts_requirement')
    <script>
        $(function() {
            $('#requirements-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('AllRequirementData') !!}',
                columns: [
                    { data: 'name_job', name: 'name_job' },
                    { data: 'name_area', name: 'name_area' },
                    { data: 'state', name: 'state' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'view', name: 'view', orderable: false, searchable: false},
                    { data: 'edit', name: 'edit', orderable: false, searchable: false},
                    { data: 'delete', name: 'delete', orderable: false, searchable: false}

                ]
            });
        });
    </script>
@endpush