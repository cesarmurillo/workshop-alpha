@extends('template.main')

@section('title', 'Solicitudes Aceptadas')
@section('section', 'Recursos Humanos')
@section('content')
    <div class="table-responsive">
        <table class="table" id="requirements-table">
            <thead>
            <tr>
                <th>Puesto</th>
                <th>Estado</th>
                <th>Fecha</th>
                <th style="width: 67px; text-align:center">Ver</th>
                <th style="width: 67px; text-align:center">Editar</th>
                <th style="width: 67px; text-align:center">Eliminar</th>
            </tr>
            </thead>

        </table>
    </div>

@endsection
@push('scripts')
    <script>
        $(function() {
            $('#requirements-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('PendingRequirementData') !!}',
                columns: [
                    { data: 'name_job', name: 'name_job' },
                    { data: 'state', name: 'state' },
                    { data: 'created_at', name: 'created_at' },

                ]
            });
        });
    </script>
@endpush