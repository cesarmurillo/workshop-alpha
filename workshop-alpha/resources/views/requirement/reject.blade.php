{!! Form::open([ 'method'  => 'delete', 'route' => [ 'requirement.destroy', $id ] ]) !!}
<center>
    <button type="submit" class="btn btn-warning m-b-10 m-l-5"><i class="fa fa-edit"></i></button>
</center>
{!! Form::close() !!}