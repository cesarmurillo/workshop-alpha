@extends('template.main')

@section('title', 'Bolsa de Trabajo')
@section('section', 'Recursos Humanos')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-primary">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">{{ $data[0]->name_job }}</h4>
                </div>
                <div class="card-body">
                        <div class="form-body">
                            <h3 class="box-title m-t-40">Resumen del puesto</h3>
                            <hr>

                            <h5>{{ $data[0]->resume_job }}</h5>

                            <h3 class="box-title m-t-40">Caracterizticas del puesto</h3>
                            <hr>

                            <h5>{{ $data[0]->specs_job }}</h5>

                            <h3 class="box-title m-t-40">Responsabilidades</h3>
                            <hr>

                            <h5>{{ $data[0]->responsibility_job }}</h5>

                            <h3 class="box-title m-t-40">Perfil profesional</h3>
                            <hr>

                            <h5>{{ $data[0]->training_job }}</h5>

                        </div>


                </div>


                <div class="card-footer">
                    {!! Form::open(['url' => 'postulant', 'id' => 'postulant-form' , 'enctype' => 'multipart/form-data']) !!}
                    <div class="form-group">
                        <label for="file">Enviar Curriculum Vitae</label>
                        <input type="file" name="cv" class="form-control-file" id="cv">
                        <input name="requirement_id" class="form-control-file" value="{{ $data[0]->id }}" hidden>
                    </div>
                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i>Postular</button>
                    {!! Form::close() !!}
                </div>


            </div>
        </div>
    </div>

@endsection