<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('template/images/favicon.png') }}">
    <title>Ela - Bootstrap Admin Dashboard Template</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('template/css/lib/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('template/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('template/css/style.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
</head>

<body class="fix-header fix-sidebar mini-sidebar">
<!-- Preloader - style you can find in spinners.css -->
<div class="preloader" style="display: none;">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg>
</div>
<!-- Main wrapper  -->
<div id="main-wrapper">

    <div class="unix-login">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="login-content card">
                        <div class="login-form">
                            <h4>Registrate</h4>
                            {!! Form::open(['url' => 'person', 'id' => 'person-form']) !!}
                            <div class="form-group row">
                                    <input type="text" class="form-control " id="first_name" name="first_name" aria-describedby="first_name-help" placeholder="Nombre" autocomplete="off">
                            </div>
                            <div class="form-group row">
                                <input type="text" class="form-control " id="last_name" name="last_name" aria-describedby="last_name-help" placeholder="Apellido Paterno" autocomplete="off">
                            </div>
                            <div class="form-group row">
                                <input type="text" class="form-control" id="second_last_name" name="second_last_name" aria-describedby="second_last_name-help" placeholder="Apellido Materno" autocomplete="off">
                            </div>
                            <div class="form-group row">
                                <input type="email" class="form-control" id="personal_email" name="personal_email" aria-describedby="second_last_name-help" placeholder="Correo Electronico" autocomplete="off">
                            </div>
                            <div class="form-group row">
                                <input type="date" class="form-control " id="birth_date" name="birth_date" placeholder="Fecha de Nacimiento" autocomplete="off">
                                <small id="cell_number-help" class="form-text text-muted">Fecha de nacimiento.</small>
                            </div>


                            <button type="submit" class="btn btn-info"> <i class="fa fa-check"></i>Registrar</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- End Wrapper -->
<!-- All Jquery -->
<script src="{{ asset('template/js/lib/jquery/jquery-3.2.0.js') }}"></script>

<script src="http://malsup.github.com/jquery.form.js"></script>

<!-- Bootstrap tether Core JavaScript -->
<script src="{{ asset('template/js/lib/bootstrap/js/popper.min.js') }}"></script>
<script src="{{ asset('template/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{ asset('template/js/jquery.slimscroll.js') }}"></script>
<!--Menu sidebar -->
<script src="{{ asset('template/js/sidebarmenu.js') }}"></script>
<!--stickey kit -->
<script src="{{ asset('template/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
<!--Custom JavaScript -->
<script src="{{ asset('template/js/custom.min.js') }}"></script>
<!--Jquery validate-->
<script src="{{ asset('template/js/lib/jquery/jquery.validate.js') }}"></script>
<script src="{{ asset('js/validate.js') }}"></script>
<script src="{{ asset('js/registerForm.js') }}"></script>
<!--Data Tables-->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!--Ajax Forms-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>



</body>
</html>