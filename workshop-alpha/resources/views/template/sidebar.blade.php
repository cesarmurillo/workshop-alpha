<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li class="nav-label">Inicio</li>
                @can('viewStaffRequirement', Auth::user())
                @endcan
                <li> <a class="" href="#" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Noticias</span></a></li>
                <li> <a class="" href="#" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Comunicados</span></a></li>


                <li class="nav-label">Recursos Humanos</li>
                <li> <a class="" href="#" aria-expanded="false"><i class="fa fa-suitcase"></i><span class="hide-menu">Convocatorias</span></a>
                </li>
                @can('viewJobRequirement', Auth::user())

                <li> <a class="" href="{{ url('jobRequirement') }}" aria-expanded="false"><i class="fa fa-suitcase"></i><span class="hide-menu">Bolsa de trabajo</span></a>
                </li>
                @endcan
                @can('viewStaffRequest', Auth::user())

                <li class=""> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-suitcase"></i><span class="hide-menu">Requerimiento de personal</span></a>
                    <ul aria-expanded="false" class="collapse" style="height: 0px;">
                        <li><a href="{{ url('allRequirement') }}">Solicitudes pendientes</a></li>
                        <li><a href="{{ url('') }}">Historial</a></li>
                    </ul>
                </li>
                @endcan
                @can('viewStaffRequirement', Auth::user())
                <li class=""> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-suitcase"></i><span class="hide-menu">Solicitud de personal</span></a>
                    <ul aria-expanded="false" class="collapse" style="height: 0px;">
                        <li><a href="{{ url('requirement/create') }}">Nueva solicitud</a></li>
                        <li><a href="{{ url('requirement') }}">Todas las solicitudes</a></li>
                        <li><a href="{{ url('acceptedRequirement') }}">Solicitudes aceptadas</a></li>
                        <li><a href="{{ url('pendingRequirement') }}">Solicitudes pendientes</a></li>
                        <li><a href="{{ url('rejectedRequirement') }}">Solicitudes rechazadas</a></li>
                    </ul>
                </li>
                @endcan


            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</div>