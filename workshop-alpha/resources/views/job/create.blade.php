@extends('template.main')

@section('title', 'Creación de Puestos de Trabajo')
@section('content')
    {!! Form::open(['url' => 'job', 'id' => 'job-form']) !!}
    <div class="form-group ">
        <input type="text" class="form-control" id="name_job" name="name_job" aria-describedby="job-help" placeholder="Ingrese el puesto de trabajo" autocomplete="off">
        <small id="job-help" class="form-text text-muted">El puesto de trabajo que desea crear</small>
    </div>

    <div class="form-group ">
        <textarea type="text" class="form-control-plaintext" id="resume_job" name="resume_job" aria-describedby="resume_job-help" placeholder="Resumen del puesto de trabajo" autocomplete="off" rows="3"></textarea>
        <small id="resume_job-help" class="form-text text-muted">Resumen general del puesto de trabajo a crear</small>
    </div>

    <div class="form-group ">
        <textarea type="text" class="form-control-plaintext" id="specs_job" name="specs_job" aria-describedby="specs_job-help" placeholder="Caracterizticas del puesto de trabajo" autocomplete="off" rows="3"></textarea>
        <small id="resume_job-help" class="form-text text-muted">Las caracterizticas especificas del puesto de trabajo</small>
    </div>

    <div class="form-group ">
        <textarea type="text" class="form-control-plaintext" id="condition_job" name="condition_job" aria-describedby="condition_job-help" placeholder="Condiciones del puesto de trabajo" autocomplete="off" rows="3"></textarea>
        <small id="condition_job-help" class="form-text text-muted">Las condiciones de trabajo relacionadas con salud, seguridad y calidad de vida en el empleo </small>
    </div>

    <div class="form-group ">
        <textarea type="text" class="form-control-plaintext" id="duty_job" name="duty_job" aria-describedby="duty_job-help" placeholder="Deberes principales del trabajo" autocomplete="off" rows="3"></textarea>
        <small id="condition_job-help" class="form-text text-muted">Los deberes que debera cumplir el empleado.</small>
    </div>

    <div class="form-group ">
        <textarea type="text" class="form-control-plaintext" id="responsibility_job" name="responsibility_job" aria-describedby="responsibility_job-help" placeholder="Responsabilidades primordiales del trabajo" autocomplete="off" rows="3"></textarea>
        <small id="condition_job-help" class="form-text text-muted">Las obligaciones a las cuales el empleado debera responder.</small>
    </div>

    <div class="form-group ">
        <textarea type="text" class="form-control-plaintext" id="training_job" name="training_job" aria-describedby="training_job-help" placeholder="Formacion profesional" autocomplete="off" rows="3"></textarea>
        <small id="condition_job-help" class="form-text text-muted">Requisitos profesionales y academicos para ejercer el puesto de trabajo.</small>
    </div>

    <div class="form-group">
        <label for="area_id">Area:</label>
        <select class="form-control valid " id="area_id" name="area_id" aria-required="true" aria-describedby="" aria-invalid="">
            <option value="" disabled selected>Seleccione uno</option>
            @foreach($areas as $area)
                <option value="{{ $area->id }}">{{ $area->name_area }}</option>
            @endforeach
        </select>
        <small id="area-help" class="form-text text-muted " >El area al cual pertenece el puesto de trabajo.</small>
    </div>

    <div class="form-group">

        <label for="">Reporta a:</label>
        <button type="button" class="btn btn-info btn-flat btn-addon btn-sm m-b-10 m-l-5" id="btn1"><i class="ti-plus"></i>Añadir</button>

            <div class=""></div>
                <div id="xd">

                </div>

    </div>


    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i>Registrar</button>
    {!! Form::close() !!}
@endsection

@push('scripts')
    <script>
        var aa=0;
        $("#btn1").click(function(){

            $("#xd").append("<div id='"+aa+"' class='form-group row'><div class=\"col-md-6\"><select class=\"form-control \" id=\"area_id\" name=\"area_id\" aria-required=\"true\" aria-describedby=\"\" aria-invalid=\"\">\n" +
                "            <option value=\"\" disabled selected>Seleccione uno</option>\n" +
                "            @foreach($areas as $area)\n" +
                "                <option value=\"{{ $area->id }}\">{{ $area->name_area }}</option>\n" +
                "            @endforeach\n" +
                "        </select></div>" +

                "<button id='btn2' data-id='\"+aa+\"' type=\"button\" class=\"btn btn-danger btn-flat btn-addon btn-sm m-b-10 m-l-5\"><i class=\"ti-close\"></i></button></div>")
            aa++;

        });
        $('#xd').on('click','#btn2',function() {
            $(this).parent().remove();
        });
    </script>
@endpush

