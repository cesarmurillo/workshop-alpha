<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('example', function () {
    return view('template.main');
});
Route::get('main', function () {
    return view('index');
});
Route::resource('privilege', 'PrivilegeController');
Route::get('getPrivilegeData', 'PrivilegeController@getPrivilegeData')->name('PrivilegeData');

Route::resource('role', 'RoleController');
Route::get('getRoleData', 'RoleController@getRoleData')->name('RoleData');

Route::resource('area', 'AreaController');
Route::get('getAreaData', 'AreaController@getAreaData')->name('AreaData');

Route::resource('job', 'JobController');
Route::get('getJobData', 'JobController@getJobData')->name('JobData');

Route::resource('employee', 'EmployeeController');
Route::get('getEmployeeData', 'EmployeeController@getEmployeeData')->name('EmployeeData');

Route::resource('person', 'PersonController');
Route::get('getPersonData', 'PersonController@getPersonData')->name('PersonData');

Route::resource('user', 'UserController');
Route::get('changePassword', 'UserController@changePasswordView');
Route::put('changePassword', 'UserController@changePassword')->name('changePassword');

Route::get('register', 'PersonController@create')->name('register');

Route::get('login', 'UserController@loginView')->name('loginView');
Route::post('login', 'UserController@login')->name('login');

Route::post('logout','UserController@logout')->name('logout');

Route::resource('requirement', 'RequirementController');
Route::get('acceptedRequirement', function () {
    return view('requirement.acceptedRequirements');
});
Route::get('pendingRequirement', function () {
    return view('requirement.pendingRequirement');
});
Route::get('rejectedRequirement', function () {
    return view('requirement.rejectedRequirement');
});

Route::get('allRequirement', function () {
    return view('requirement.allRequirements');
});

Route::get('jobRequirement', function () {
    return view('requirement.jobRequirement');
});

Route::get('getRequirementData', 'RequirementController@getRequirementData')->name('RequirementData');
Route::get('getAcceptedRequirementData', 'RequirementController@getAcceptedRequirementData')->name('AcceptedRequirementData');
Route::get('getPendingRequirementData', 'RequirementController@getPendingRequirementData')->name('PendingRequirementData');
Route::get('getRejectedRequirementData', 'RequirementController@getRejectedRequirementData')->name('RejectedRequirementData');

Route::get('getAllRequirementData', 'RequirementController@getAllRequirementData')->name('AllRequirementData');

Route::get('jobRequirementData', 'RequirementController@jobRequirement')->name('JobRequirementData');

Route::post('accept', 'RequirementController@accept')->name('accept');

Route::resource('postulant', 'PostulantController');