$(document).ready(function() {
submit();

});

function error1(xhr, status, error) {
    alert(xhr.responseText)
}
function validateForm()  {
    $("#privilege-form").validate({
        rules: {
            privilege: {
                required: true,
                minlength: 4,
            }
        }
    });
}

function success() {
    $.alert({
        title: 'Alert!',
        content: 'Simple alert!',
    });
}
function submit() {
    var options = {
        target:        '',   // target element(s) to be updated with server response
        beforeSubmit: validateForm(),  // pre-submit callback
        success: '',  // post-submit callback
        error: '',
        // other available options:
        //url:       url         // override for form's 'action' attribute
        //type:      type        // 'get' or 'post', override for form's 'method' attribute
        //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)
        //clearForm: true        // clear all form fields after successful submit
        //resetForm: true        // reset the form after successful submit

        // $.ajax options can be used here too, for example:
        //timeout:   3000
    };

    // bind to the form's submit event
    $('#privilege-form').submit(function() {
        // inside event callbacks 'this' is the DOM element so we first
        // wrap it in a jQuery object and then invoke ajaxSubmit

        $(this).ajaxSubmit(options);

        // !!! Important !!!
        // always return false to prevent standard browser submit and page navigation
        return false;
    });
}