$().ready(function () {
    $("#role-form").validate({
        rules: {
            role: {
                required: true,
                minlength: 4,
            },
            'privilege[]':{
                required:true,
            }
        }
    })
});