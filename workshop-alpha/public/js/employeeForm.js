$(document).ready(function() {
    submit();

});

function error1(xhr, status, error) {
    alert(xhr.responseText)
}
function validateForm()  {
    $("#employee-form").validate({
        rules: {
            first_name:{
                required: true,
                minlength: 4,
            },
            last_name:{
                required: true,
                minlength: 4,
            },
            second_last_name:{
                required: true,
                minlength: 4,
            },
            gender:{
                required: true,
            },
            birth_date:{
                required: true,
                date: true
            },
            personal_email:{
                required: true,
                minlength: 4,
                email: true
            },
            phone_number:{
                required: true,
                minlength: 4,
            },
            cell_number:{
                required: true,
                minlength: 4,
            },
            address:{
                required: true,
                minlength: 4,
            },
            birth_place:{
                required: true,
                minlength: 4,
            },
            nacionality:{
                required: true,
                minlength: 4,
            },
            civil_status:{
                required: true,
            },
        }
    });
}

function success() {
    $.confirm({
        title: 'Exito!',
        content: 'Se a creado el nuevo registro de manera exitosa',
        buttons: {
            confirm: function () {
                location.href = 'http://127.0.0.1:8000/employee/';
            },
        }
    });
}
function submit() {
    var options = {
        target: '',   // target element(s) to be updated with server response
        beforeSubmit: validateForm(),  // pre-submit callback
        success: success,  // post-submit callback
        error: '',
        // other available options:
        //url:       url         // override for form's 'action' attribute
        //type:      type        // 'get' or 'post', override for form's 'method' attribute
        //dataType:  null        // 'xml', 'script', or 'json' (expected server response type)
        //clearForm: true        // clear all form fields after successful submit
        //resetForm: true        // reset the form after successful submit

        // $.ajax options can be used here too, for example:
        //timeout:   3000
    };

    // bind to the form's submit event
    $('#employee-form').submit(function() {
        // inside event callbacks 'this' is the DOM element so we first
        // wrap it in a jQuery object and then invoke ajaxSubmit

        $(this).ajaxSubmit(options);

        // !!! Important !!!
        // always return false to prevent standard browser submit and page navigation
        return false;
    });
}